package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.PagamentoDTO;
import br.com.cartoes.models.Fatura;
import br.com.cartoes.services.FaturaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;


@RestController
@RequestMapping("/fatura")
public class FaturaController {


    @Autowired
    private FaturaService faturaService;

    @PostMapping("/{cliente_id}/{cartao_id}/pagar")
    @ResponseStatus(HttpStatus.CREATED)
    @Transactional
    public Fatura pagarFaturaDoCartao(@PathVariable(name = "cliente_id") int clienteId, @PathVariable(name = "cartao_id") int cartaoId) {
        try {
            Fatura faturaPaga = faturaService.pagarFaturaDoCartao(clienteId, cartaoId);
            return faturaPaga;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping("/{cliente_id}/{cartao_id}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> buscarFaturaDoClientePorCartao(@PathVariable(name = "cliente_id") int clienteId, @PathVariable(name = "cartao_id") int cartaoId) {
        try {
            List<PagamentoDTO> fatura = faturaService.buscarFaturaDoClientePorCartao(clienteId, cartaoId);
            return fatura;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
