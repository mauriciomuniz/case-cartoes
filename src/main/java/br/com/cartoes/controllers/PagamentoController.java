package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.PagamentoDTO;
import br.com.cartoes.services.PagamentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class PagamentoController {

    @Autowired
    private PagamentoService pagamentoService;

    @PostMapping("/pagamento")
    @ResponseStatus(HttpStatus.CREATED)
    public PagamentoDTO efetuarPagamento(@RequestBody @Valid PagamentoDTO pagamentoDTO) {
        try {
            PagamentoDTO pagamentoRealizado = pagamentoService.efetuarPagamento(pagamentoDTO);
            return pagamentoRealizado;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @GetMapping("/pagamentos/{id_cartao}")
    @ResponseStatus(HttpStatus.OK)
    public List<PagamentoDTO> buscarPagamentoPorId(@PathVariable (name = "id_cartao") int idCartao) {
        try {
            List<PagamentoDTO> pagamentos = pagamentoService.buscarPagamentoPorIdCartao(idCartao);
            return pagamentos;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
