package br.com.cartoes.controllers;

import br.com.cartoes.DTOs.CartaoDTO;
import br.com.cartoes.DTOs.CartaoGetDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.services.CartaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/cartao")
public class CartaoController {

    @Autowired
    private CartaoService cartaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CartaoDTO cadastrarCartao(@RequestBody @Valid CartaoDTO cartaoDTO) {
        try {
            CartaoDTO retornoCartaoDTO = cartaoService.cadastrarCartao(cartaoDTO);
            return retornoCartaoDTO;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }

    }

    @PatchMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoDTO ativarCartao (@PathVariable(name = "numero") String numero, @RequestBody CartaoDTO cartaoDTO) {
        return cartaoService.ativarCartao(numero, cartaoDTO);
    }

    @GetMapping("/{numero}")
    @ResponseStatus(HttpStatus.OK)
    public CartaoGetDTO buscarCartaoPorID(@PathVariable(name = "numero") int numero) {
        try {
            CartaoGetDTO cartaoGetDTO = cartaoService.buscarCartaoPorNumero(numero);
            return cartaoGetDTO;
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }
}
