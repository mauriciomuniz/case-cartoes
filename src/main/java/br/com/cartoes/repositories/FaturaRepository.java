package br.com.cartoes.repositories;

import br.com.cartoes.models.Fatura;
import org.springframework.data.repository.CrudRepository;

public interface FaturaRepository extends CrudRepository<Fatura, Integer> {
}
