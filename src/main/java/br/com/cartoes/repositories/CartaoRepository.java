package br.com.cartoes.repositories;

import br.com.cartoes.models.Cartao;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface CartaoRepository extends CrudRepository<Cartao, Integer> {
    Cartao findByNumero(int numeroCartao);

    boolean existsByIdAndClienteId(int cartaoId, int clienteId);
}
