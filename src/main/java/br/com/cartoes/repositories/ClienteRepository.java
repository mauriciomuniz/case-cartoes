package br.com.cartoes.repositories;

import br.com.cartoes.models.Cliente;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
}
