package br.com.cartoes.services;

import br.com.cartoes.DTOs.PagamentoDTO;
import br.com.cartoes.models.Fatura;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.FaturaRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class FaturaService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private FaturaRepository faturaRepository;

    public Fatura pagarFaturaDoCartao(int clienteId, int cartaoId) {
        Fatura fatura = new Fatura();

        List<PagamentoDTO> pagamentosDTO = buscarFaturaDoClientePorCartao(clienteId, cartaoId);

        if (!pagamentosDTO.isEmpty()) {
            fatura = retornaReciboFatura(pagamentosDTO);

            deletarRegistrosPagamento(cartaoId);

            return salvarFatura(fatura);
        } else {
            throw new RuntimeException("Não existem pagamentos a serem realizados");
        }
    }


    public List<PagamentoDTO> buscarFaturaDoClientePorCartao(int clienteId, int cartaoId) {
        if (cartaoRepository.existsByIdAndClienteId(cartaoId, clienteId)) {

            List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(cartaoId);

            return montaSaidaPagamento(pagamentos);
        } else {
            throw new RuntimeException("Combinação id cliente e id cartão não encontrada");
        }
    }

    public List<PagamentoDTO> montaSaidaPagamento(List<Pagamento> pagamentos) {
        List<PagamentoDTO> pagamentosDTO = new ArrayList<>();
        for (Pagamento aux : pagamentos) {
            PagamentoDTO pagamentoDTO = new PagamentoDTO();

            pagamentoDTO.setId(aux.getId());
            pagamentoDTO.setValor(aux.getValor());
            pagamentoDTO.setCartao_id(aux.getCartao().getId());
            pagamentoDTO.setDescricao(aux.getDescricao());
            pagamentosDTO.add(pagamentoDTO);
        }
        return pagamentosDTO;
    }

    public Fatura retornaReciboFatura(List<PagamentoDTO> pagamentosDTO) {
        Fatura faturaPaga = new Fatura();
        LocalDate date = LocalDate.now();
        double valorPago = 0;
        for (PagamentoDTO pagamentoDTO : pagamentosDTO) {
            valorPago += pagamentoDTO.getValor();
        }
        faturaPaga.setValorPago(valorPago);
        faturaPaga.setPagoEm(date);

        return faturaPaga;
    }

    public void deletarRegistrosPagamento(int cartaoId) {
        pagamentoRepository.deleteByCartaoId(cartaoId);
    }

    public Fatura salvarFatura(Fatura fatura) {
        return faturaRepository.save(fatura);
    }
}
