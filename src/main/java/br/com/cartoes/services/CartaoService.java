package br.com.cartoes.services;

import br.com.cartoes.DTOs.CartaoDTO;
import br.com.cartoes.DTOs.CartaoGetDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Cliente;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.ClienteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.Optional;

@Service
public class CartaoService {

    @Autowired
    private CartaoRepository cartaoRepository;

    @Autowired
    private ClienteRepository clienteRepository;

    public CartaoDTO cadastrarCartao(CartaoDTO cartaoDTO) {

        Optional<Cliente> clienteOptional = clienteRepository.findById(cartaoDTO.getClienteId());

        if (clienteOptional.isPresent()) {
            Cartao cartao = new Cartao();
            Cliente cliente = clienteOptional.get();
            CartaoDTO saidaCartaoDTO;
            int numeroCartao = Integer.parseInt(cartaoDTO.getNumero());

            cartao.setNumero(numeroCartao);
            cartao.setAtivo(false);
            cartao.setCliente(cliente);
            cartao = cartaoRepository.save(cartao);

            saidaCartaoDTO = montaDTO(cartao);
            return saidaCartaoDTO;
        } else {
            throw new RuntimeException("Não existe cliente com o Id informado");
        }
    }

    public CartaoDTO ativarCartao(String numero, CartaoDTO cartaoDTO) {
        Cartao cartao = new Cartao();
        CartaoGetDTO retornoCartao;
        CartaoDTO saidaCartaoDTO = new CartaoDTO();
        Cliente cliente = new Cliente();
        int numeroCartao = Integer.parseInt(numero);

        retornoCartao = buscarCartaoPorNumero(numeroCartao);

        cliente.setId(retornoCartao.getClienteId());
        cartao.setCliente(cliente);
        cartao.setId(retornoCartao.getId());
        cartao.setNumero(Integer.parseInt(retornoCartao.getNumero()));
        cartao.setAtivo(cartaoDTO.isAtivo());
        cartao = cartaoRepository.save(cartao);

        saidaCartaoDTO = montaDTO(cartao);
        return saidaCartaoDTO;
    }

    public CartaoGetDTO buscarCartaoPorNumero(int numero) {
        CartaoGetDTO cartaoGetDTO = new CartaoGetDTO();
        Optional<Cartao> cartaoOptional = Optional.ofNullable(cartaoRepository.findByNumero(numero));
        if (cartaoOptional.isPresent()) {
            cartaoGetDTO.setId(cartaoOptional.get().getId());
            cartaoGetDTO.setClienteId(cartaoOptional.get().getCliente().getId());
            cartaoGetDTO.setNumero(String.valueOf(cartaoOptional.get().getNumero()));
            return cartaoGetDTO;
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }
    }

    public CartaoDTO montaDTO(Cartao cartao) {
        CartaoDTO cartaoDTO = new CartaoDTO();

        cartaoDTO.setNumero(String.valueOf(cartao.getNumero()));
        cartaoDTO.setClienteId(cartao.getCliente().getId());
        cartaoDTO.setId(cartao.getId());

        return cartaoDTO;
    }
}