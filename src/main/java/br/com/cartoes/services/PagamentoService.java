package br.com.cartoes.services;

import br.com.cartoes.DTOs.PagamentoDTO;
import br.com.cartoes.models.Cartao;
import br.com.cartoes.models.Pagamento;
import br.com.cartoes.repositories.CartaoRepository;
import br.com.cartoes.repositories.PagamentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.management.relation.RoleUnresolved;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class PagamentoService {

    @Autowired
    private PagamentoRepository pagamentoRepository;

    @Autowired
    private CartaoRepository cartaoRepository;

    public PagamentoDTO efetuarPagamento(PagamentoDTO pagamentoDTO) {
        Pagamento pagamento = new Pagamento();
        Optional<Cartao> cartaoOptional = cartaoRepository.findById(pagamentoDTO.getCartao_id());
        if (cartaoOptional.isPresent()) {
            Cartao cartao = cartaoOptional.get();
            pagamento.setCartao(cartao);
            pagamento.setDescricao(pagamentoDTO.getDescricao());
            pagamento.setValor(pagamentoDTO.getValor());
            Pagamento pagamentoSalvo = pagamentoRepository.save(pagamento);

            return montaSaidaPagamento(pagamentoSalvo, cartao);
        } else {
            throw new RuntimeException("O cartão informado no pagamento não existe");
        }

    }

    public List<PagamentoDTO> buscarPagamentoPorIdCartao(int idCartao) {
        List<Pagamento> pagamentos = pagamentoRepository.findAllByCartaoId(idCartao);

        if (!pagamentos.isEmpty()) {
            return montaSaidaPagamento(pagamentos);
        } else {
            throw new RuntimeException("Cartão não encontrado");
        }

    }

    public PagamentoDTO montaSaidaPagamento(Pagamento pagamento, Cartao cartao) {
        PagamentoDTO pagamentoDTO = new PagamentoDTO();
        pagamentoDTO.setCartao_id(cartao.getId());
        pagamentoDTO.setDescricao(pagamento.getDescricao());
        pagamentoDTO.setValor(pagamento.getValor());
        pagamentoDTO.setId(pagamento.getId());
        return pagamentoDTO;
    }

    public List<PagamentoDTO> montaSaidaPagamento(List<Pagamento> pagamentos) {
        List<PagamentoDTO> pagamentosDTO = new ArrayList<>();
        for (Pagamento aux : pagamentos) {
            PagamentoDTO pagamentoDTO = new PagamentoDTO();

            pagamentoDTO.setId(aux.getId());
            pagamentoDTO.setValor(aux.getValor());
            pagamentoDTO.setCartao_id(aux.getCartao().getId());
            pagamentoDTO.setDescricao(aux.getDescricao());
            pagamentosDTO.add(pagamentoDTO);
        }
        return pagamentosDTO;
    }
}
